import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InputBoxTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSingleInputField() {
		System.setProperty("webdriver.chrome.driver","/Users/macstudent/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

		
		/*  
		 * 1. Find the textbox.(id = User Message)
		 * 2. Type "HELLO WORLD" into the textbox  (snedKeys("..."))
		 * 3. Find the button (cssSelectors = form#get-input button)
		 * 4. CLICK on the button (.click())
		 * 5. FIND the output message thing (id= display)
		 * 6. CHECK the output message (expectedResult = "HELLO WORLD") (.getText())
		 */

		WebElement textBox = driver.findElement(By.id("user-message"));
		textBox.sendKeys("HELLO WORLD!");
		
		WebElement button = driver.findElement(By.cssSelector("form#get-input button"));
		button.click();
		
		WebElement outputSpan = driver.findElement(By.id("display"));
		String outputMessage = outputSpan.getText();
		
		assertEquals("HELLO WORLD!",outputMessage);
		
		// At the end of test case
		driver.close();
		
	}
	
	@Test
	public void testTwoInputFields(){
		fail("Not yet implemented");
	}

}
